package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.regex.*;

@SpringBootApplication
public class DemoApplication {

	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		String txt = "2[abc]3[ab]c";
		decompress(txt);
		txt = "10[a]c2[ab]";
		decompress(txt);
		txt = "2[3[a]b]";
		decompress(txt);
	}

	public static void decompress(String txt){
		Pattern r = Pattern.compile("(\\d\\[\\w+])|(\\d{2}\\[\\w+]|\\d\\[\\d\\[\\w]\\w])|\\w");
		Matcher m = r.matcher(txt);
		String text = txt;
		String real = "";
		while(true){
			if (m.find( )) {
				txt = txt.replace(m.group(), "");
				String number = m.group().replaceAll("\\D+","");

				Pattern x = Pattern.compile("(\\d\\[\\d\\[\\w]\\w])");
				Matcher s = x.matcher(m.group());
				if(s.find( )) {
					Pattern a = Pattern.compile("(\\d\\[\\w])");
					Matcher b = a.matcher(m.group());
					b.find();
					String temp = s.group().replace(b.group(), "");
					String subNumber = temp.replaceAll("\\D+","");
					temp = temp.replace(subNumber,"").replace("[", "").replace("]","");

					for(int i=0;i<Integer.parseInt(subNumber);i++){
						number = b.group().replaceAll("\\D+","");
						real += getReal(number, b.group());
						real += temp;
					} 
				} else {
					if(number.equals("")){
						real += m.group();
					} else {
						String temp = m.group();
						real += getReal(number, temp);			
					}
				}
			}else {
				break;
			}
		}
		System.out.println("Input : "+text);
		System.out.println("Output : "+real);
	}

	public static String getReal(String number, String temp) {
		String real = "";
		temp = temp.replace(number, "").replace("[", "").replace("]", "");
		for (int i = 0; i < Integer.parseInt(number); i++) {
			real += temp;
		};
		return real;
	}
}
